# Web3 Server Signed Transaction Using Infura



## Setting up the development environment 

Before we start we need some tools and dependencies. Please install the following:

1. Node.js and npm (comes with Node)
2. Git
3. Truffle
4. MetaMask with 1 or 2 Ropsten Accounts (1 ether Minimum) (keep Metamask seed      phrase)

## Create Project in Infura

1. Go to infura.io and create a new account.
2. Create a new project
3. Note the Project ID 

## Edit Network configuration in truffle-config.js

 replace project_id and metamask_seed_phrase with your infura project id and metamask seed phrase

## Installation
Install all dependencies using following command <br />

$ npm install


## Compiling and deploying the smart contract.
Go to: http://remix.ethereum.org
1. Compile the contract :<br>
   $ truffle compile
   
2. Deploy contract to the ropsten testnet using following command:<br>
   $ truffle migrate --network ropsten


## Edit app.js
 replace project_id and metamask_seed_phrase with your infura project id and metamask seed phrase



## Run Dapp
Using following command, we can run the application<br />
$ npm start



