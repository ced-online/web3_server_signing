var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var MyContractJSON = require(path.join(__dirname, 'build/contracts/StudentManagementSystem.json'));
const HDWalletProvider = require('@truffle/hdwallet-provider');
/*-------------------WEB3 Connection starts---------------------*/

const Web3 = require('web3');
//infura project_id
const infuraKey = "project_id";
//12 word mnemonic which addresses are created from.
const mnemonic = "metamask_seed_phrase";
//it will tell the provider to manage the address at the index specified
const address_index= 0;
//it will create number addresses when instantiated
const num_addresses= 5;
 
//web3 provider
const provider = new HDWalletProvider(mnemonic, `https://ropsten.infura.io/v3/${infuraKey}`, address_index, num_addresses);
web3 = new Web3(provider);

contractAddress = MyContractJSON.networks['3'].address;
const contractAbi = MyContractJSON.abi;
SMS = new web3.eth.Contract(contractAbi, contractAddress);
/*-------------------WEB3 Connection Ends----------------------*/


var indexRouter = require('./routes/index');
var setStudentRouter = require('./routes/setStudent');
var getStudentRouter = require('./routes/getStudent');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/setStudent', setStudentRouter);
app.use('/getStudent', getStudentRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
